#include <stdio.h>
#define HEIGHT 8
#define WIDTH 8

int nBoard[HEIGHT][WIDTH];

// ������� �1
void vDecToBin(int nDec)
{
	if (nDec > 0)
	{
		int nTemp = nDec % 2;
		vDecToBin(nDec / 2);
		printf("%d", nTemp);
	}
	
	return;
}

//������� �2
int nExponent(int nA, int nB)
{
	if (nB > 0)
		return nA * nExponent(nA, nB - 1);

	return 1;
}

//������� �3
int nExpAdv(int nA, int nB)
{
	if (nB <= 0)
		return 1;
	else if (nB % 2 == 0)
		return  nExpAdv(nA * nA, nB / 2);
	else
		return nA * nExpAdv(nA, nB - 1);
}

//������� �4
int nCountRoutes(int nX, int nY)
{
	if ((nX == 0) && (nY == 0))
		return 1;
	else if (nBoard[nX][nY] == 1)
		return 0;
	else if ((nX == 0) ^ (nY == 0))
	{
		if (nX == 0)
			return nCountRoutes(nX, nY - 1);
		else
			return nCountRoutes(nX - 1, nY);
	}
	else
		return nCountRoutes(nX - 1, nY) + nCountRoutes(nX, nY - 1);
}

int main()
{
	int nDecToBin;

	scanf("%d", &nDecToBin);
	vDecToBin(nDecToBin);

	printf("\n%d\n", nExponent(2, 8));

	printf("%d\n", nExpAdv(2, 10));

	nBoard[1][0] = 1;
	nBoard[1][2] = 1;

	for (int x = 0; x < HEIGHT; x++)
	{
		for (int y = 0; y < WIDTH; y++)
			printf("%5d", nCountRoutes(x, y));

		printf("\n");
	}
	return 0;
}